package com.androidlab42.bite.flows.details

import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.usecases.FakeArticleDetailsUseCase
import com.androidlab42.bite.usecases.FakeArticleDetailsUseCaseResponses
import com.google.common.truth.Truth.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.standalone.get

class DetailsViewModelTest: BaseUnitTest() {

    private lateinit var underTest: DetailsViewModel

    @Before
    override fun startup() {
        super.startup()
    }

    @After
    override fun tearDown() {
        super.tearDown()
        underTest.onCleared()
    }

    @Test
    fun `on success search should return details in the nav state`() {
        // Given...
        underTest = DetailsViewModel(get(), get(), get())

        // When...
        underTest.search("cheeseburger")

        // Then...
        underTest.navEvents.value!!.handle {
            val event = it as NavEvents.LoadResults
            assertThat(event.abs).isEqualTo("abstract-1")
            assertThat(event.firstParagraph).isEqualTo("lead-paragraph")
            assertThat(event.headline).isEqualTo("headline")
            assertThat(event.imageUrl).isEqualTo("url")
        }
    }

    @Test
    fun `on failure search should return a reason in the nav state`() {
        // Given...
        val response = FakeArticleDetailsUseCaseResponses.FAILURE
        val repo = FakeArticleDetailsUseCase(response)
        underTest = DetailsViewModel(repo, get(), get())

        // When...
        underTest.search("cheeseburger")

        // Then...
        underTest.navEvents.value!!.handle {
            val event = it as NavEvents.Error
            assertThat(event.reason).isEqualTo("Something went wrong.")
        }
    }
}