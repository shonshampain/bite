package com.androidlab42.bite.flows.reader

import android.content.Context
import android.widget.ImageView
import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.model.nytimes.Multimedia
import com.androidlab42.bite.model.transfer.ImageRequest
import com.androidlab42.bite.usecases.FakeArticleListUseCase
import com.androidlab42.bite.usecases.FakeArticleListUseCaseResponses
import com.google.common.truth.Truth.assertThat
import com.jakewharton.rxrelay2.PublishRelay
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.standalone.get
import org.mockito.Mockito.mock

class ReaderViewModelTest: BaseUnitTest() {

    private lateinit var underTest: ReaderViewModel

    @Before
    override fun startup() {
        super.startup()
    }

    @After
    override fun tearDown() {
        super.tearDown()
        underTest.onCleared()
    }

    @Test
    fun `on success search should return details in the nav state`() {
        // Given...
        underTest = ReaderViewModel(get(), get(), get(), get())

        // When...
        underTest.search()

        // Then...
        underTest.navEvents.value!!.handle {
            it as NavEvents.LoadArticles
            assertThat(it.list.size).isEqualTo(3)
            for (i in 0..2) {
                assertThat(it.list[i].abstract).isEqualTo("abstract-${i+1}")
                assertThat(it.list[i].section).isEqualTo("section-${i+1}")
                assertThat(it.list[i].subsection).isEqualTo("subsection-${i+1}")
                assertThat(it.list[i].title).isEqualTo("title-${i+1}")
                assertThat(it.list[i].url).isEqualTo("url-${i+1}")
                assertMM(it.list[i].multimedia)
            }
        }
    }

    private fun assertMM(mm: List<Multimedia>?) {
        assertThat(mm?.size).isEqualTo(3)
        assertThat(mm?.get(0)?.height).isEqualTo(1)
        assertThat(mm?.get(0)?.width).isEqualTo(1)
        assertThat(mm?.get(0)?.subtype).isEqualTo("subtype-1")
        assertThat(mm?.get(0)?.url).isEqualTo("url-1")
        assertThat(mm?.get(1)?.height).isEqualTo(2)
        assertThat(mm?.get(1)?.width).isEqualTo(2)
        assertThat(mm?.get(1)?.subtype).isEqualTo("subtype-2")
        assertThat(mm?.get(1)?.url).isEqualTo("url-2")
        assertThat(mm?.get(2)?.height).isEqualTo(3)
        assertThat(mm?.get(2)?.width).isEqualTo(3)
        assertThat(mm?.get(2)?.subtype).isEqualTo("subtype-3")
        assertThat(mm?.get(2)?.url).isEqualTo("url-3")
    }

    @Test
    fun `image request results should be posted to the nav event`() {
        // Given...
        underTest = ReaderViewModel(get(), get(), get(), get())
        val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
        val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
        val mm3 = Multimedia(height = 140, width = 210, subtype = "subtype-3", url = "url")
        val ir = ImageRequest(listOf(mm1, mm2, mm3), ImageView(mock(Context::class.java)))
        val o = PublishRelay.create<ImageRequest>()

        // When...
        underTest.handleImageRequests(o)
        o.accept(ir)

        // Then...
        underTest.navEvents.value!!.handle {
            it as NavEvents.LoadImage
            assertThat(it.url).isEqualTo("url")
        }
    }

    @Test
    fun `details request results should be posted to the nav event`() {
        // Given
        underTest = ReaderViewModel(get(), get(), get(), get())
        val o = PublishRelay.create<String>()

        // When...
        underTest.handleDetailsRequests(o)
        o.accept("foo")

        // Then...
        underTest.navEvents.value!!.handle {
            it as NavEvents.LoadDetails
            assertThat(it.url).isEqualTo("foo")
        }
    }

    @Test
    fun `request completion results should be posted to the nav event`() {
        // Given
        underTest = ReaderViewModel(get(), get(), get(), get())
        val o = PublishRelay.create<Boolean>()

        // When...
        underTest.handleRequestCompletions(o)
        o.accept(true)

        // Then...
        underTest.navEvents.value!!.handle {
            it as NavEvents.RequestComplete
        }
    }

    @Test
    fun `on failure search should return a reason in the nav state`() {
        // Given...
        val response = FakeArticleListUseCaseResponses.FAILURE
        val useCase = FakeArticleListUseCase(response)
        underTest = ReaderViewModel(useCase, get(), get(), get())

        // When...
        underTest.search()

        // Then...
        underTest.navEvents.value!!.handle {
            val event = it as NavEvents.Error
            assertThat(event.reason).isEqualTo("Something went wrong.")
        }
    }
}