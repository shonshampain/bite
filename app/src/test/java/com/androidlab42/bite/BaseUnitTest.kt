package com.androidlab42.bite

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext

open class BaseUnitTest : KoinComponent {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    open fun startup() {
        StandAloneContext.startKoin(listOf(testModules))
    }

    @After
    open fun tearDown() {
        StandAloneContext.stopKoin()
    }
}