package com.androidlab42.bite.usecases

import com.androidlab42.bite.BaseUnitTest
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.koin.standalone.get

class SearchParamsUseCaseTest : BaseUnitTest() {

    private lateinit var underTest: SearchParamsUseCaseIF

    @Before
    override fun startup() {
        super.startup()
        underTest = SearchParamsUseCase(get())
    }

    @Test
    fun `the source should be properly initialized`() {
        // Given...

        // When...

        // Then...
        assertThat(underTest.source).isEqualTo("all")
    }

    @Test
    fun `the section should be properly initialized`() {
        // Given...

        // When...

        // Then...
        assertThat(underTest.section).isEqualTo("all")
    }

    @Test
    fun `the source should be saved`() {
        // Given...
        assertThat(underTest.source).isNotEqualTo("foo-bar")

        // When...
        underTest.source = "foo-bar"

        // Then...
        assertThat(underTest.source).isEqualTo("foo-bar")
    }

    @Test
    fun `the section should be saved`() {
        // Given...
        assertThat(underTest.section).isNotEqualTo("hey-ya")

        // When...
        underTest.source = "hey-ya"

        // Then...
        assertThat(underTest.source).isEqualTo("hey-ya")
    }
}
