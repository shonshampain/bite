package com.androidlab42.bite.usecases

import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.repos.FakeArticleDetailsRepo
import com.androidlab42.bite.repos.FakeArticleDetailsRepoResponses
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.koin.standalone.get

class ArticleDetailsUseCaseTest : BaseUnitTest() {

    private lateinit var underTest: ArticleDetailsUseCaseIF

    @Test
    fun `when the use case returns success the search result should be a success with a correct structure`() {
        // Given...
        underTest = ArticleDetailsUseCase(get(), get())
        val test = underTest.searchResult.test()

        // When...
        underTest.search("cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(true)
            assertThat(it.values()[0].response?.status).isEqualTo("status")
            assertThat(it.values()[0].response?.copyright).isEqualTo("copyright")
            assertThat(it.values()[0].response?.response?.docs?.size).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.leadParagraph).isEqualTo("lead-paragraph")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.headline?.main).isEqualTo("headline")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.size).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.height).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.width).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.subtype).isEqualTo("subtype-1")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.url).isEqualTo("url-1")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.height).isEqualTo(2)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.width).isEqualTo(2)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.subtype).isEqualTo("subtype-2")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.url).isEqualTo("url-2")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.height).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.width).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.subtype).isEqualTo("xlarge")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.url).isEqualTo("url")
        }
    }

    @Test
    fun `when the use case returns an error the search result should be a failure with a reason`() {
        // Given...
        val repo = FakeArticleDetailsRepo(FakeArticleDetailsRepoResponses.FAILURE)
        underTest = ArticleDetailsUseCase(repo, get())
        val test = underTest.searchResult.test()

        // When...
        underTest.search("cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(false)
            assertThat(it.values()[0].reason).isEqualTo("Something went wrong.")
        }
    }
}