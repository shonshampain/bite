package com.androidlab42.bite.usecases

import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.model.nytimes.Multimedia
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class PreferredBannerImageUseCaseTest : BaseUnitTest() {

    private lateinit var underTest: PreferredBannerImageUseCaseIF
    private val mm1 = Multimedia(height = 1, width = 1, url = "url1", subtype = "foo")
    private val mm2 = Multimedia(height = 1, width = 1, url = "url2", subtype = "bar")
    private val mm3 = Multimedia(height = 1, width = 1, url = "url3", subtype = "hey")
    private val mm4 = Multimedia(height = 1, width = 1, url = "url4", subtype = "joe")
    private val mm5 = Multimedia(height = 1, width = 1, url = "url5", subtype = "xlarge")

    @Before
    override fun startup() {
        super.startup()
        underTest = PreferredBannerImageUseCase()
    }

    @Test
    fun `searching with a null list should return a null result`() {
        // Given...

        // When...
        val result = underTest.findImage(null)

        // Then...
        assertThat(result).isNull()
    }

    @Test
    fun `searching with an empty list should return a null result`() {
        // Given...

        // When...
        val result = underTest.findImage(listOf())

        // Then...
        assertThat(result).isNull()
    }

    @Test
    fun `searching with a list that does not contain the correct type should return a null result`() {
        // Given...

        // When...
        val result = underTest.findImage(listOf(mm1, mm2, mm3, mm4))

        // Then...
        assertThat(result).isNull()
    }

    @Test
    fun `searching with a list that does contain the correct type should return the correct url`() {
        // Given...

        // When...
        val result = underTest.findImage(listOf(mm1, mm2, mm5, mm3, mm4))

        // Then...
        assertThat(result).isEqualTo("url5")
    }
}