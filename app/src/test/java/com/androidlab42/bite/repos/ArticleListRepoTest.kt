package com.androidlab42.bite.repos

import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.model.FakeAPIManagerResponses
import com.androidlab42.bite.model.FakeApiManager
import com.androidlab42.bite.model.nytimes.Multimedia
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.koin.standalone.get

class ArticleListRepoTest : BaseUnitTest() {

    private lateinit var underTest: ArticleListRepo

    @Test
    fun `a successful search should return a search results with success and the data structure`() {
        // Given...
        underTest = ArticleListRepo(FakeApiManager(), get())
        val test = underTest.articleListResponse.test()

        // When...
        underTest.search("food", "cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(true)
            assertThat(it.values()[0].response?.status).isEqualTo("status")
            assertThat(it.values()[0].response?.copyright).isEqualTo("copyright")
            assertThat(it.values()[0].response?.results?.size).isEqualTo(3)
            for (i in 0..2) {
                assertThat(it.values()[0].response?.results?.get(i)?.abstract).isEqualTo("abstract-${i+1}")
                assertThat(it.values()[0].response?.results?.get(i)?.section).isEqualTo("section-${i+1}")
                assertThat(it.values()[0].response?.results?.get(i)?.subsection).isEqualTo("subsection-${i+1}")
                assertThat(it.values()[0].response?.results?.get(i)?.title).isEqualTo("title-${i+1}")
                assertThat(it.values()[0].response?.results?.get(i)?.url).isEqualTo("url-${i+1}")
                assertMM(it.values()[0].response?.results?.get(i)?.multimedia)
            }
        }
    }

    private fun assertMM(mm: List<Multimedia>?) {
        assertThat(mm?.size).isEqualTo(3)
        assertThat(mm?.get(0)?.height).isEqualTo(1)
        assertThat(mm?.get(0)?.width).isEqualTo(1)
        assertThat(mm?.get(0)?.subtype).isEqualTo("subtype-1")
        assertThat(mm?.get(0)?.url).isEqualTo("url-1")
        assertThat(mm?.get(1)?.height).isEqualTo(2)
        assertThat(mm?.get(1)?.width).isEqualTo(2)
        assertThat(mm?.get(1)?.subtype).isEqualTo("subtype-2")
        assertThat(mm?.get(1)?.url).isEqualTo("url-2")
        assertThat(mm?.get(2)?.height).isEqualTo(3)
        assertThat(mm?.get(2)?.width).isEqualTo(3)
        assertThat(mm?.get(2)?.subtype).isEqualTo("subtype-3")
        assertThat(mm?.get(2)?.url).isEqualTo("url-3")
    }

    @Test
    fun `a failed search should return a search results with failure and the reason`() {
        // Given...
        val response = FakeAPIManagerResponses.FAILURE
        underTest = ArticleListRepo(FakeApiManager(response), get())
        val test = underTest.articleListResponse.test()

        // When...
        underTest.search("food", "cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(false)
            assertThat(it.values()[0].reason).isEqualTo("Something went wrong.")
            assertThat(it.values()[0].response).isNull()
        }
    }
}