package com.androidlab42.bite.repos

import com.androidlab42.bite.BaseUnitTest
import com.androidlab42.bite.model.FakeAPIManagerResponses
import com.androidlab42.bite.model.FakeApiManager
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.koin.standalone.get

class ArticleDetailsRepoTest: BaseUnitTest() {

    private lateinit var underTest: ArticleDetailsRepo

    @Test
    fun `a successful search should return a search results with success and the data structure`() {
        // Given...
        underTest = ArticleDetailsRepo(FakeApiManager(), get())
        val test = underTest.articleDetailsResponse.test()

        // When...
        underTest.search("cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(true)
            assertThat(it.values()[0].response?.status).isEqualTo("status")
            assertThat(it.values()[0].response?.copyright).isEqualTo("copyright")
            assertThat(it.values()[0].response?.response?.docs?.size).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.leadParagraph).isEqualTo("lead-paragraph")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.headline?.main).isEqualTo("headline")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.size).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.height).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.width).isEqualTo(1)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.subtype).isEqualTo("subtype-1")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(0)?.url).isEqualTo("url-1")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.height).isEqualTo(2)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.width).isEqualTo(2)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.subtype).isEqualTo("subtype-2")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(1)?.url).isEqualTo("url-2")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.height).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.width).isEqualTo(3)
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.subtype).isEqualTo("subtype-3")
            assertThat(it.values()[0].response?.response?.docs?.get(0)?.multimedia?.get(2)?.url).isEqualTo("url-3")
        }
    }

    @Test
    fun `a failed search should return a search results with failure and the reason`() {
        // Given...
        val response = FakeAPIManagerResponses.FAILURE
        underTest = ArticleDetailsRepo(FakeApiManager(response), get())
        val test = underTest.articleDetailsResponse.test()

        // When...
        underTest.search("cheeseburger")

        // Then...
        test.assertOf {
            assertThat(it.values().size).isEqualTo(1)
            assertThat(it.values()[0].result).isEqualTo(false)
            assertThat(it.values()[0].reason).isEqualTo("Something went wrong.")
            assertThat(it.values()[0].response).isNull()
        }
    }
}