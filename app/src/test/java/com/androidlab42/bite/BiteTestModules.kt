package com.androidlab42.bite

import com.androidlab42.bite.flows.details.DetailsViewModel
import com.androidlab42.bite.flows.reader.ReaderViewModel
import com.androidlab42.bite.model.ApiManagerIF
import com.androidlab42.bite.model.FakeApiManager
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.repos.*
import com.androidlab42.bite.usecases.*
import io.reactivex.schedulers.Schedulers
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val testModules: Module = module {

    // Misc
    single { FakeApiManager() as ApiManagerIF }
    single { RxSchedulers(Schedulers.trampoline(), Schedulers.trampoline()) }

    // Use Cases
    single { FakeArticleListUseCase() as ArticleListUseCaseIF }
    single { FakeArticleDetailsUseCase() as ArticleDetailsUseCaseIF }
    single { PreferredThumbnailImageUseCase() as PreferredThumbnailImageUseCaseIF }
    single { PreferredBannerImageUseCase() as PreferredBannerImageUseCaseIF }
    single { SearchParamsUseCase(get()) as SearchParamsUseCaseIF }

    // Repos
    single { FakeArticleDetailsRepo() as ArticleDetailsRepoIF }
    single { FakeArticleListRepo() as ArticleListRepoIF }
    single { FakeSharedPrefsRepo() as SharedPrefsRepoIF }

    // ViewModels
    viewModel { DetailsViewModel(get(), get(), get()) }
    viewModel { ReaderViewModel(get(), get(), get(), get()) }
}