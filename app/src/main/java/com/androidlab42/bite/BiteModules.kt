package com.androidlab42.bite

import com.androidlab42.bite.flows.details.DetailsViewModel
import com.androidlab42.bite.flows.reader.ReaderViewModel
import com.androidlab42.bite.model.ApiManager
import com.androidlab42.bite.model.ApiManagerIF
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.repos.*
import com.androidlab42.bite.usecases.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val modules: Module = module {

    // Misc
    single { ApiManager() as ApiManagerIF }
    single { RxSchedulers() }

    // Use Cases
    single { ArticleListUseCase(get(), get()) as ArticleListUseCaseIF }
    single { ArticleDetailsUseCase(get(), get()) as ArticleDetailsUseCaseIF }
    single { PreferredThumbnailImageUseCase() as PreferredThumbnailImageUseCaseIF }
    single { PreferredBannerImageUseCase() as PreferredBannerImageUseCaseIF }
    single { SearchParamsUseCase(get()) as SearchParamsUseCaseIF }

    // Repos
    single { ArticleDetailsRepo(get(), get()) as ArticleDetailsRepoIF }
    single { ArticleListRepo(get(), get()) as ArticleListRepoIF }
    single { SharedPrefsRepo(androidContext()) as SharedPrefsRepoIF }

    // ViewModels
    viewModel { DetailsViewModel(get(), get(), get()) }
    viewModel { ReaderViewModel(get(), get(), get(), get()) }
}
