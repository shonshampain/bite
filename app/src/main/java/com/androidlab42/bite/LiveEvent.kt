package com.androidlab42.bite

import androidx.lifecycle.Observer

/**
 * Helper class for ensuring the integrity of Activity/VM Navigation.
 * It's necessary to ensure Navigation Events only happen once.
 */
open class LiveEvent<out T>(private val content: T) {
    private var hasBeenHandled = false

    fun handle(handler: (T) -> Unit) {
        if (!hasBeenHandled) {
            hasBeenHandled = true
            handler(content)
        }
    }
}

/**
 * Helper class for observing a LiveEvent, making the code more compact.
 */
class ActionObserver<T>(private val handler: (T) -> Unit) : Observer<LiveEvent<T>> {
    override fun onChanged(value: LiveEvent<T>?) {
        value?.handle(handler)
    }
}
