package com.androidlab42.bite.flows.reader

import androidx.recyclerview.widget.DiffUtil
import com.androidlab42.bite.model.nytimes.Article

class ReaderDiffer : DiffUtil.ItemCallback<Article>() {

    override fun areItemsTheSame(oldArticle: Article, newArticle: Article): Boolean {
        return oldArticle.title == newArticle.title
    }

    override fun areContentsTheSame(oldArticle: Article, newArticle: Article): Boolean {
        return oldArticle == newArticle
    }
}