package com.androidlab42.bite.flows.reader

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidlab42.bite.ActionObserver
import com.androidlab42.bite.R
import com.androidlab42.bite.databinding.ActivityReaderBinding
import com.androidlab42.bite.flows.details.DetailsActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

class ReaderActivity : AppCompatActivity() {

    private val vm by viewModel<ReaderViewModel>()
    private lateinit var binding: ActivityReaderBinding
    private val readerAdapter = ReaderAdapter()
    private lateinit var sourceAdapter: ArrayAdapter<String>
    private lateinit var sectionAdapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding()
        setupAdapter()
        setupSourceSpinner()
        setupSectionSpinner()
        setupNavigation()
        setupRefresh()
        binding.sourceSpinner.setSelection(sourceAdapter.getPosition(vm.source))
        binding.sectionSpinner.setSelection(sectionAdapter.getPosition(vm.section))
    }

    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reader)
        binding.lifecycleOwner = this
        binding.viewState = vm.viewState
    }

    /**
     * Wire up the VM to field adapter requests.
     */
    private fun setupAdapter() {
        vm.handleImageRequests(readerAdapter.imageRequests)
        vm.handleDetailsRequests(readerAdapter.detailsRequests)
        vm.handleRequestCompletions(readerAdapter.completionRequests)

        binding.recyclerView.apply {
            adapter = readerAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    /**
     * The source spinner is a fixed value spinner allowing the user to select known NY Times sources.
     * Sources are not user-friendly in their description, so are mapped to friendly text.
     */
    private fun setupSourceSpinner() {
        val sourceValues = mapOf(
            getString(R.string.all_nyt) to "all",
            getString(R.string.print_nyt) to "nyt",
            getString(R.string.internet_nyt) to "inyt"
        )
        sourceAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, sourceValues.keys.toList())
        binding.sourceSpinner.adapter = sourceAdapter
        binding.sourceSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos) as String
                vm.source = sourceValues[item] ?: "all"
                vm.search()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    /**
     * The section spinner is a fixed value spinner allowing the user to select known NY Times sections.
     */
    private fun setupSectionSpinner() {
        sectionAdapter = ArrayAdapter(this,
            android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.nyt_sections))
        binding.sectionSpinner.adapter = sectionAdapter
        binding.sectionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
                val item = parent.getItemAtPosition(pos) as String
                vm.section = item
                vm.search()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    /**
     * Observe the VM Navigation object.
     *
     * LoadArticles indicates a successful search.
     * LoadImage indicates a request for an ImageView to be loaded via Glide.
     * LoadDetails indicates that the user has tapped on a story.
     * RequestComplete indicates that the search operation is complete; OK to remove spinner.
     * Error indicates the search was not successful.
     */
    private fun setupNavigation() {
        vm.navEvents.observe(this, ActionObserver {
            when (it) {
                is NavEvents.LoadArticles -> {
                    readerAdapter.set(it.list)
                }
                is NavEvents.NoResults -> {
                    readerAdapter.set()
                }
                is NavEvents.LoadImage -> {
                    val requestOptions = RequestOptions()
                    requestOptions.placeholder(R.drawable.nytimes_logo)
                    Glide.with(this)
                        .load(it.url)
                        .apply(requestOptions)
                        .into(it.image)
                }
                is NavEvents.LoadDetails -> {
                    DetailsActivity.launchActivity(this, it.url)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                }
                is NavEvents.RequestComplete -> {
                    binding.swipeRefresh.isRefreshing = false
                }
                is NavEvents.Error -> {
                    Snackbar.make(binding.layout, getString(R.string.error), Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun setupRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            vm.search()
        }
    }
}
