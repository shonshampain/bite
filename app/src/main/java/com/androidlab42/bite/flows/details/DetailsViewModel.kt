package com.androidlab42.bite.flows.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.androidlab42.bite.LiveEvent
import com.androidlab42.bite.model.ApiManager
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.usecases.ArticleDetailsUseCaseIF
import com.androidlab42.bite.usecases.PreferredBannerImageUseCaseIF
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * Navigation class driving Activity state changes.
 * LoadResults is posted when a successful search occurs.
 * Error is posted when an error has occurred.
 */
sealed class NavEvents {
    data class LoadResults(val headline: String, val abs: String, val firstParagraph: String, val imageUrl: String?) : NavEvents()
    data class Error(val reason: String) : NavEvents()
}

class DetailsViewModel(private val articleDetailsUseCase: ArticleDetailsUseCaseIF,
                       private val preferredBannerImageUseCase: PreferredBannerImageUseCaseIF,
                       rxSchedulers: RxSchedulers) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val _navEvents = MutableLiveData<LiveEvent<NavEvents>>()
    /**
     * Navigation object which the Activity will observe.
     */
    val navEvents: LiveData<LiveEvent<NavEvents>> = _navEvents

    init {
        compositeDisposable += articleDetailsUseCase
            .searchResult
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({ response ->
                if (response.result) {
                    _navEvents.value = LiveEvent(
                        NavEvents.LoadResults(
                            headline = response?.response?.response?.docs?.get(0)?.headline?.main ?: "Unknown",
                            abs = response?.response?.response?.docs?.get(0)?.`abstract` ?: "Unknown",
                            firstParagraph = response?.response?.response?.docs?.get(0)?.leadParagraph ?: "Unknown",
                            imageUrl = preferredBannerImageUseCase.findImage(response?.response?.response?.docs?.get(0)?.multimedia
                            )
                        )
                    )
                } else {
                    Timber.d("[details] error: ${response.reason}")
                    _navEvents.value = LiveEvent(
                        NavEvents.Error(response?.reason ?: "Unknown")
                    )
                }
            }, {
                Timber.e(it, "[details] Cannot search article details")
            })
    }

    /**
     * Get the Details for a given NY Times story [url].
     * Results are posted to Navigation.
     */
    fun search(url: String) {
        articleDetailsUseCase.search(url)
    }

    fun glideBase(): String {
        return ApiManager.NYT_GLIDE_BASE
    }

    public override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}