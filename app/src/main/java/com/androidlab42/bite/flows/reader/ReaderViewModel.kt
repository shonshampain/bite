package com.androidlab42.bite.flows.reader

import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.androidlab42.bite.LiveEvent
import com.androidlab42.bite.R
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.model.nytimes.Article
import com.androidlab42.bite.model.transfer.ImageRequest
import com.androidlab42.bite.usecases.ArticleListUseCaseIF
import com.androidlab42.bite.usecases.PreferredThumbnailImageUseCaseIF
import com.androidlab42.bite.usecases.SearchParamsUseCaseIF
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import java.util.*

/**
 * The NY Times Article API can be searched via source and sections.
 */
data class ViewState(
    val source: (context: Context) -> String = { "" },
    val section: (context: Context) -> String = { "" },
    val noResults: Boolean = false
)

sealed class NavEvents {
    data class LoadArticles(val list: List<Article>) : NavEvents()
    object NoResults : NavEvents()
    data class LoadImage(val url: String?, val image: ImageView) : NavEvents()
    data class LoadDetails(val url: String): NavEvents()
    object RequestComplete : NavEvents()
    data class Error(val reason: String): NavEvents()
}

class ReaderViewModel(private val articleListUseCase: ArticleListUseCaseIF,
                      private val preferredThumbnailImageUseCase: PreferredThumbnailImageUseCaseIF,
                      private val searchParamsUseCase: SearchParamsUseCaseIF,
                      private val rxSchedulers: RxSchedulers) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    /**
     * Flow (reader) view state object.
     * As mentioned in activity_reader.xml, there is no reason to search the strings in this manner.
     * However, it serves as a good example of ViewState usage, and safely passing context into the VM.
     */
    private val _viewState = MutableLiveData<ViewState>().apply { value =
        ViewState(
            source = { context -> context.getString(R.string.source)} ,
            section = { context -> context.getString(R.string.section)}
        )
    }
    /**
     * The actual view state object that the xml will bind to.
     */
    val viewState: LiveData<ViewState> = _viewState

    private val _navEvents = MutableLiveData<LiveEvent<NavEvents>>()
    val navEvents: LiveData<LiveEvent<NavEvents>> = _navEvents

    var source: String
        set(value) { searchParamsUseCase.source = value }
        get() = searchParamsUseCase.source

    var section: String
        set(value) { searchParamsUseCase.section = value }
        get() = searchParamsUseCase.section

    init {
        compositeDisposable += articleListUseCase
            .searchResult
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({ response ->
                if (response.result) {
                    Timber.d("[search] vm: results size ${response.response?.results?.size}")
                    response.response?.results?.let {
                        if (it.isEmpty()) {
                            _viewState.value = _viewState.value!!.copy( noResults = true )
                            _navEvents.value = LiveEvent(
                                NavEvents.NoResults
                            )
                        } else {
                            _viewState.value = _viewState.value!!.copy( noResults = false )
                            _navEvents.value = LiveEvent(
                                NavEvents.LoadArticles(it)
                            )
                        }
                    } ?: noResults()
                } else {
                    Timber.e("[search] vm: results error ${response.reason}")
                    _navEvents.value = LiveEvent(
                        NavEvents.Error(response?.reason ?: "Unknown")
                    )
                }
            }, {
                Timber.e(it, "[search] Cannot search articles ${it.message}")
            })
    }

    private fun noResults() {
        _viewState.value = _viewState.value!!.copy( noResults = true )
        _navEvents.value = LiveEvent(
            NavEvents.NoResults
        )
    }

    /**
     * The adapter has requested an image to be loaded, use Activity Navigation to field this.
     */
    fun handleImageRequests(o: Observable<ImageRequest>) {
        compositeDisposable += o
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                _navEvents.value = LiveEvent(
                    NavEvents.LoadImage(url = preferredThumbnailImageUseCase.findImage(it.multimedia), image = it.imageView)
                )
            }, {
                Timber.e(it, "Cannot handle image request")
            })
    }

    /**
     * The user has clicked on an article, requesting details.
     * Use Activity Navigation to field this.
     */
    fun handleDetailsRequests(o: Observable<String>) {
        compositeDisposable += o
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                _navEvents.value = LiveEvent(
                    NavEvents.LoadDetails(url = it)
                )
            }, {
                Timber.e(it, "Cannot handle article detail request")
            })
    }

    /**
     * The current search request has completed,
     * Use Activity Navigation to remove the spinner.
     */
    fun handleRequestCompletions(o: Observable<Boolean>) {
        compositeDisposable += o
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                _navEvents.value = LiveEvent(
                    NavEvents.RequestComplete
                )
            }, {
                Timber.e(it, "Cannot handle request completion")
            })
    }

    /**
     * Perform a search against the NY Times Article API.
     */
    fun search() {
        Timber.d("[search] vm: [${searchParamsUseCase.source}] [${searchParamsUseCase.section}]")
        articleListUseCase.search(searchParamsUseCase.source, searchParamsUseCase.section.toLowerCase(Locale.getDefault()))
    }

    public override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}