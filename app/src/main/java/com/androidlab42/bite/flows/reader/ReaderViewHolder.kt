package com.androidlab42.bite.flows.reader

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidlab42.bite.R

class ReaderViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    val title: TextView = view.findViewById(R.id.title)
    val imageView: ImageView = view.findViewById(R.id.image_view)
}
