package com.androidlab42.bite.flows.reader

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.androidlab42.bite.R
import com.androidlab42.bite.model.nytimes.Article
import com.androidlab42.bite.model.transfer.ImageRequest
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

/**
 * Adapter to hold a list of NY Times articles.
 * When a view has an image to display, the request will be posted to the imageRequests observable.
 * When the view has been clicked on, the request will be posted to the detailsRequests observable.
 * When the list is finished updating via .set(), a notice will be posted to completionRequests.
 */
class ReaderAdapter : RecyclerView.Adapter<ReaderViewHolder>() {

    private val differ: AsyncListDiffer<Article> = AsyncListDiffer(this, ReaderDiffer())

    private val _imageRequests: PublishRelay<ImageRequest> = PublishRelay.create()
    val imageRequests: Observable<ImageRequest> = _imageRequests

    private val _detailsRequests: PublishRelay<String> = PublishRelay.create()
    val detailsRequests: Observable<String> = _detailsRequests

    private val _completionRequests: PublishRelay<Boolean> = PublishRelay.create()
    val completionRequests: Observable<Boolean> = _completionRequests

    fun set(list: List<Article> = listOf()) {
        differ.submitList(list)
        _completionRequests.accept(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReaderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.article_item, parent, false)
        return ReaderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ReaderViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.title.text = item.title
        _imageRequests.accept(
            ImageRequest(
                item.multimedia,
                holder.imageView
            )
        )
        holder.view.setOnClickListener {
            _detailsRequests.accept(item.url)
        }
    }
}