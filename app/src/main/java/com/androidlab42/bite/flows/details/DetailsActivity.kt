package com.androidlab42.bite.flows.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.androidlab42.bite.ActionObserver
import com.androidlab42.bite.R
import com.androidlab42.bite.databinding.ActivityDetailsBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

private const val URL = "URL"

class DetailsActivity : AppCompatActivity() {

    companion object {
        fun launchActivity(context: Context, url: String) {
            val i = Intent(context, DetailsActivity::class.java)
            i.putExtra(URL, url)
            context.startActivity(i)
        }
    }

    private val vm by viewModel<DetailsViewModel>()
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataBinding()
        setupNavigation()
        vm.search(intent.getStringExtra(URL))
    }

    private fun setupDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.lifecycleOwner = this
    }

    /**
     * Observe the VM Navigation object.
     * LoadResults indicates a successful search.
     * Error indicates the search was not successful.
     */
    private fun setupNavigation() {
        vm.navEvents.observe(this, ActionObserver {
            when (it) {
                is NavEvents.LoadResults -> {
                    binding.progress.visibility = View.GONE
                    binding.separator.visibility = View.VISIBLE
                    binding.headline.text = it.headline
                    binding.abs.text = it.abs
                    binding.content.text = it.firstParagraph
                    val requestOptions = RequestOptions()
                    requestOptions.placeholder(R.drawable.nytimes_logo)
                    Glide.with(this)
                        .load("${vm.glideBase()}${it.imageUrl}")
                        .apply(requestOptions)
                        .into(binding.image)
                }
                is NavEvents.Error -> {
                    Snackbar.make(binding.layout, getString(R.string.error), Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }
}