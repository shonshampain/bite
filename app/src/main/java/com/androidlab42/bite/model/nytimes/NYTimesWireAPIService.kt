package com.androidlab42.bite.model.nytimes

import com.androidlab42.bite.model.ApiManager.Companion.NYT_API_KEY
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NYTimesWireAPIServiceIF {

    @GET("/svc/news/v3/content/{source}/{section}.json")
    fun search(
        @Path("source") source: String,
        @Path("section") section: String,
        @Query("api-key") apiKey: String = NYT_API_KEY
    ): Single<WireAPISearchResponse>

}