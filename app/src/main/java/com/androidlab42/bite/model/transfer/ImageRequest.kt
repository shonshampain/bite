package com.androidlab42.bite.model.transfer

import android.widget.ImageView
import com.androidlab42.bite.model.nytimes.Multimedia

data class ImageRequest(val multimedia: List<Multimedia>?, val imageView: ImageView)