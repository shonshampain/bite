package com.androidlab42.bite.model.nytimes

data class Headline(
    val main: String
)