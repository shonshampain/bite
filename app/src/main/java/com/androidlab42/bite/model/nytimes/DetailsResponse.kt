package com.androidlab42.bite.model.nytimes

data class DetailsResponse(
    val docs: List<Doc>
)