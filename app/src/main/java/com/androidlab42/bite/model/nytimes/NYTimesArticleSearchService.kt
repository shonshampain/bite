package com.androidlab42.bite.model.nytimes

import com.androidlab42.bite.model.ApiManager.Companion.NYT_API_KEY
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface NYTimesArticleSearchServiceIF {

    @GET("/svc/search/v2/articlesearch.json")
    fun search(
        @Query("fq") fq: String = "web_url:(\"\")",
        @Query("api-key") apiKey: String = NYT_API_KEY
    ): Single<ArticleDetailsResponse>

}