package com.androidlab42.bite.model

import com.androidlab42.bite.BuildConfig
import com.androidlab42.bite.model.nytimes.*
import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val READ_TIMEOUT = 30L // seconds
private const val CONNECT_TIMEOUT = 30L // seconds
private const val WRITE_TIMEOUT = 30L // seconds
private const val NY_TIMES_BASE_URL = "https://api.nytimes.com/"

interface ApiManagerIF {
    /**
     * This value contains a Retrofit object configured for the NY Times Wire API.
     * This API is used to get the headlines in various sections.
     */
    val nyTimesWireAPIService: NYTimesWireAPIServiceIF
    /**
     * This value contains a Retrofit object configured for the NY Times Article Search API.
     * As a general rule, The NY Times is not going to give out free content,
     * which makes designing a "details" page difficult.
     * However, when you use this API, you get the lead paragraph, which is good for this demo.
     */
    val nyTimesArticleSearchService: NYTimesArticleSearchServiceIF
}

class ApiManager : ApiManagerIF {

    companion object {
        const val NYT_API_KEY = "kYzGTfJElqE9bGfUZ6TDeoWXG53VvYXa"
        const val NYT_GLIDE_BASE = "https://static01.nyt.com/"
    }


    override val nyTimesWireAPIService: NYTimesWireAPIServiceIF
        get() = getRetrofit().create(NYTimesWireAPIServiceIF::class.java)

    override val nyTimesArticleSearchService: NYTimesArticleSearchServiceIF
        get() = getRetrofit().create(NYTimesArticleSearchServiceIF::class.java)

    /**
     * Construct the OkHTTP client, adding a logging interceptor so that logcat shows the calls.
     */
    private fun getClient(): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .followRedirects(true)
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(interceptor)
        }
        return clientBuilder.build()
    }

    /**
     * Construct the Retrofit object for a given base url.
     */
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NY_TIMES_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(getClient())
            .build()
    }
}

enum class FakeAPIManagerResponses { SUCCESS, FAILURE }

class FakeApiManager(response: FakeAPIManagerResponses = FakeAPIManagerResponses.SUCCESS): ApiManagerIF {
    override val nyTimesWireAPIService: NYTimesWireAPIServiceIF = FakeNYTimesWireAPIService(response)
    override val nyTimesArticleSearchService: NYTimesArticleSearchServiceIF = FakeNYTimesArticleSearchService(response)
}

class FakeNYTimesWireAPIService(private val response: FakeAPIManagerResponses = FakeAPIManagerResponses.SUCCESS) : NYTimesWireAPIServiceIF {
    override fun search(source: String, section: String, apiKey: String): Single<WireAPISearchResponse> {
        return when(response) {
            FakeAPIManagerResponses.SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "subtype-3", url = "url-3")
                val article1 = Article("section-1", "subsection-1", "title-1", "abstract-1", "url-1", listOf(mm1, mm2,mm3))
                val article2 = Article("section-2", "subsection-2", "title-2", "abstract-2", "url-2", listOf(mm1, mm2,mm3))
                val article3 = Article("section-3", "subsection-3", "title-3", "abstract-3", "url-3", listOf(mm1, mm2,mm3))
                val response = WireAPISearchResponse("status", "copyright", 500, listOf(article1, article2, article3))
                Single.just(response)
            }
            FakeAPIManagerResponses.FAILURE -> {
                Single.error<WireAPISearchResponse>(Throwable("Something went wrong."))
            }
        }
    }
}

class FakeNYTimesArticleSearchService(private val response: FakeAPIManagerResponses = FakeAPIManagerResponses.SUCCESS) : NYTimesArticleSearchServiceIF {
    override fun search(fq: String, apiKey: String): Single<ArticleDetailsResponse> {
        return when(response) {
            FakeAPIManagerResponses.SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "subtype-3", url = "url-3")
                val headline1 = Headline(main = "headline")
                val doc1 = Doc("lead-paragraph", headline1, listOf(mm1, mm2,mm3), "abstract-1")
                val details = DetailsResponse(listOf(doc1))
                val response = ArticleDetailsResponse("status", "copyright", details)
                Single.just(response)
            }
            FakeAPIManagerResponses.FAILURE -> {
                Single.error<ArticleDetailsResponse>(Throwable("Something went wrong."))
            }
        }
    }
}
