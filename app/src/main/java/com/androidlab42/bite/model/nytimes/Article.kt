package com.androidlab42.bite.model.nytimes

data class Article(
    val section: String,
    val subsection: String,
    val title: String,
    val abstract: String,
    val url: String,
    val multimedia: List<Multimedia>
)