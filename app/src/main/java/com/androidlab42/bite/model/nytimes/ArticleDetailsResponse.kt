package com.androidlab42.bite.model.nytimes

data class ArticleDetailsResponse(
    val status: String,
    val copyright: String,
    val response: DetailsResponse
)