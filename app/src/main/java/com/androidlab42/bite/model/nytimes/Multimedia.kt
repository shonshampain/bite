package com.androidlab42.bite.model.nytimes

data class Multimedia(
    val url: String,
    val width: Int,
    val height: Int,
    val subtype: String
)