package com.androidlab42.bite.model

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * This class is intended to provide the schedulers used for Rx calls.
 * It is meant to be injected so that tests can be configured with Schedulers.trampoline.
 */
data class RxSchedulers(
    val observe: Scheduler = AndroidSchedulers.mainThread(),
    val subscribe: Scheduler = io.reactivex.schedulers.Schedulers.io()
)