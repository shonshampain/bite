package com.androidlab42.bite.model.nytimes

import com.google.gson.annotations.SerializedName

data class Doc(
    @SerializedName("lead_paragraph")
    val leadParagraph: String,
    val headline: Headline,
    val multimedia: List<Multimedia>,
    val `abstract`: String
)