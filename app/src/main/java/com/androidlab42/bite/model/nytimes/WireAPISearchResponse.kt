package com.androidlab42.bite.model.nytimes

import com.google.gson.annotations.SerializedName

data class WireAPISearchResponse(val status: String,
                                 val copyright: String,
                                 @SerializedName("num_results")
                                 val numResults: Int,
                                 val results: List<Article>)