package com.androidlab42.bite

import android.app.Application
import org.koin.android.ext.android.startKoin
import timber.log.Timber

// App is Definitely used in AndroidManifest.xml
@Suppress("unused", "UNUSED_PARAMETER")
open class BiteApplication : Application() {

    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        }
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }

        startKoin(this, listOf(modules))
    }
}