package com.androidlab42.bite.usecases

import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.model.nytimes.Article
import com.androidlab42.bite.model.nytimes.Multimedia
import com.androidlab42.bite.model.nytimes.WireAPISearchResponse
import com.androidlab42.bite.repos.ArticleListRepoIF
import com.androidlab42.bite.repos.ArticleListResult
import com.androidlab42.bite.usecases.FakeArticleListUseCaseResponses.FAILURE
import com.androidlab42.bite.usecases.FakeArticleListUseCaseResponses.SUCCESS
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * This use case encapsulates the logic to perform a search using the Article List Repo.
 */
interface ArticleListUseCaseIF {
    /**
     * Search the Article List API in a given [source] and [section].
     * Nothing is returned, as the call is reactive and will place results on the observable.
     */
    fun search(source: String, section: String)
    /**
     * The observable containing the search results.
     */
    val searchResult: Observable<ArticleListResult>
}

class ArticleListUseCase(private val articleListRepo: ArticleListRepoIF,
                         rxSchedulers: RxSchedulers) : ArticleListUseCaseIF {

    private val compositeDisposable = CompositeDisposable()

    private val _searchResult: PublishRelay<ArticleListResult> = PublishRelay.create()
    override val searchResult: Observable<ArticleListResult> = _searchResult

    init {
        compositeDisposable += articleListRepo.articleListResponse
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({ response ->
                if (response.result) {
                    Timber.d("[search] use case: results size ${response.response?.results?.size}")
                    _searchResult.accept(response)
                } else {
                    Timber.d("[search] use case: results error ${response.reason}")
                    _searchResult.accept(ArticleListResult(result = false, reason = response.reason))
                }
            }, {
                Timber.e("[search] use case: list error ${it.message}")
            })
    }

    override fun search(source: String, section: String) {
        Timber.d("[search] use case: [$source] [$section]")
        articleListRepo.search(source, section)
    }
}

enum class FakeArticleListUseCaseResponses { SUCCESS, FAILURE }

class FakeArticleListUseCase(private val response: FakeArticleListUseCaseResponses = SUCCESS) : ArticleListUseCaseIF {

    private val _searchResult: BehaviorRelay<ArticleListResult> = BehaviorRelay.create()
    override val searchResult: Observable<ArticleListResult> = _searchResult

    override fun search(source: String, section: String) {
        when (response) {
            SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "subtype-3", url = "url-3")
                val article1 = Article("section-1", "subsection-1", "title-1", "abstract-1", "url-1", listOf(mm1, mm2,mm3))
                val article2 = Article("section-2", "subsection-2", "title-2", "abstract-2", "url-2", listOf(mm1, mm2,mm3))
                val article3 = Article("section-3", "subsection-3", "title-3", "abstract-3", "url-3", listOf(mm1, mm2,mm3))
                val wasr = WireAPISearchResponse("status", "copyright", 500, listOf(article1, article2, article3))
                _searchResult.accept(ArticleListResult(true, response = wasr))
            }
            FAILURE -> {
                _searchResult.accept(ArticleListResult(false, reason = "Something went wrong."))
            }
        }
    }
}