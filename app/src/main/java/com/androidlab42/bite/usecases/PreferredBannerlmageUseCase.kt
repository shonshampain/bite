package com.androidlab42.bite.usecases

import com.androidlab42.bite.model.nytimes.Multimedia

private const val PREFERRED_IMAGE_SUBTYPE = "xlarge"

/**
 * This use case encapsulates the logic for selecting a banner image from among a list of choices.
 */
interface PreferredBannerImageUseCaseIF {
    /**
     * Given the NY Times (list of) Multimedia object(s), choose the appropriate image, or null
     * if none is found.
     * @return the url for the image, or null if no appropriate image is found.
     */
    fun findImage(imageList: List<Multimedia>?): String?
}

class PreferredBannerImageUseCase : PreferredBannerImageUseCaseIF {

    override fun findImage(imageList: List<Multimedia>?): String? {
        if (imageList == null) {
            return null
        }
        imageList.forEach {
            if (it.subtype == PREFERRED_IMAGE_SUBTYPE) {
                return it.url
            }
        }
        return null
    }
}