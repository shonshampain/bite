package com.androidlab42.bite.usecases

import com.androidlab42.bite.repos.SharedPrefsRepoIF

private const val SOURCE = "SOURCE"
private const val SECTION = "SECTION"

private const val DEFAULT_SOURCE = "all"
private const val DEFAULT_SECTION = "all"

/**
 * This use case is responsible for persisting search parameters.
 */
interface SearchParamsUseCaseIF {
    /**
     * The NY Times [source].
     */
    var source: String
    /**
     * The NY Times [section].
     */
    var section: String
}

/**
 * Implementation details: save the params into Shared Prefs.
 */
class SearchParamsUseCase(private val sharedPrefsRepo: SharedPrefsRepoIF): SearchParamsUseCaseIF {

    override var source: String
        set(value) = sharedPrefsRepo.putString(SOURCE, value)
        get() = sharedPrefsRepo.getString(SOURCE, "")

    override var section: String
        set(value) = sharedPrefsRepo.putString(SECTION, value)
        get() = sharedPrefsRepo.getString(SECTION, "")

    init {
        if (source.isEmpty()) {
            source = DEFAULT_SOURCE
        }
        if (section.isEmpty()) {
            section = DEFAULT_SECTION
        }
    }
}