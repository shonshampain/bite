package com.androidlab42.bite.usecases

import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.model.nytimes.*
import com.androidlab42.bite.repos.ArticleDetailsRepoIF
import com.androidlab42.bite.repos.ArticleDetailsResult
import com.androidlab42.bite.usecases.FakeArticleDetailsUseCaseResponses.FAILURE
import com.androidlab42.bite.usecases.FakeArticleDetailsUseCaseResponses.SUCCESS
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

/**
 * This use case encapsulates the logic to perform a search using the Article Details Repo.
 */
interface ArticleDetailsUseCaseIF {
    /**
     * Search the Article Details API for a given [url].
     * Nothing is returned, as the call is reactive and will place results on the observable.
     */
    fun search(url: String)
    /**
     * The observable containing the search results.
     */
    val searchResult: Observable<ArticleDetailsResult>
}

class ArticleDetailsUseCase(private val articleDetailsRepo: ArticleDetailsRepoIF,
                            rxSchedulers: RxSchedulers
) : ArticleDetailsUseCaseIF {

    private val compositeDisposable = CompositeDisposable()

    private val _searchResult: PublishRelay<ArticleDetailsResult> = PublishRelay.create()
    override val searchResult: Observable<ArticleDetailsResult> = _searchResult

    init {
        compositeDisposable += articleDetailsRepo.articleDetailsResponse
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({ response ->
                if (response.result) {
                    _searchResult.accept(response)
                } else {
                    _searchResult.accept(ArticleDetailsResult(result = false, reason = response.reason))

                }
            }, {
                Timber.e("[search] use case: details error ${it.message}")
            })
    }

    override fun search(url: String) {
        articleDetailsRepo.search(url)
    }
}

enum class FakeArticleDetailsUseCaseResponses { SUCCESS, FAILURE }

class FakeArticleDetailsUseCase(private val response: FakeArticleDetailsUseCaseResponses = SUCCESS) : ArticleDetailsUseCaseIF {

    private val _searchResult: BehaviorRelay<ArticleDetailsResult> = BehaviorRelay.create()
    override val searchResult: Observable<ArticleDetailsResult> = _searchResult

    override fun search(url: String) {
        when (response) {
            SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "xlarge", url = "url")
                val headline1 = Headline(main = "headline")
                val doc1 = Doc("lead-paragraph", headline1, listOf(mm1, mm2,mm3), "abstract-1")
                val details = DetailsResponse(listOf(doc1))
                val adr = ArticleDetailsResponse("status", "copyright", details)
                _searchResult.accept(ArticleDetailsResult(true, response = adr))
            }
            FAILURE -> {
                _searchResult.accept(ArticleDetailsResult(false, reason = "Something went wrong."))
            }
        }
    }
}