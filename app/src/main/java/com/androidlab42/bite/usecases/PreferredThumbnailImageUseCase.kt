package com.androidlab42.bite.usecases

import com.androidlab42.bite.model.nytimes.Multimedia

private const val PREFERRED_IMAGE_WIDTH = 210
private const val PREFERRED_IMAGE_HEIGHT = 140

/**
 * This use case encapsulates the logic for selecting a thumbnail image from among a list of choices.
 */
interface PreferredThumbnailImageUseCaseIF {
    /**
     * Given the NY Times (list of) Multimedia object(s), choose the appropriate image, or null
     * if none is found.
     * @return the url for the image, or null if no appropriate image is found.
     */
    fun findImage(imageList: List<Multimedia>?): String?
}

class PreferredThumbnailImageUseCase : PreferredThumbnailImageUseCaseIF {

    override fun findImage(imageList: List<Multimedia>?): String? {
        if (imageList == null) {
            return null
        }
        imageList.forEach {
            if (it.height == PREFERRED_IMAGE_HEIGHT && it.width == PREFERRED_IMAGE_WIDTH) {
                return it.url
            }
        }
        return null
    }
}