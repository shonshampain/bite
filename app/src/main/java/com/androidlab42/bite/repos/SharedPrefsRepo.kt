package com.androidlab42.bite.repos

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

private const val PREFERENCES_NAME = "BitePreferences"

/**
 * Repo wrapping Shared Preferences.
 * The repo uses .commit() preferentially as the benefits of being able to use the
 * value right away (as is the case in synchronous operations) WAY outweigh any
 * performance gained by .apply()'ing in the background.
 */
interface SharedPrefsRepoIF {
    fun putBoolean(name: String, value: Boolean)
    fun getBoolean(name: String, default: Boolean = false): Boolean
    fun putInt(name: String, value: Int)
    fun getInt(name: String, default: Int = -1): Int
    fun putString(name: String, value: String)
    fun getString(name: String, default: String = ""): String
}

@SuppressLint("ApplySharedPref") // .apply is evil
class SharedPrefsRepo(context: Context): SharedPrefsRepoIF {

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    override fun putBoolean(name: String, value: Boolean) {
        prefs.edit().putBoolean(name, value).commit()
    }

    override fun getBoolean(name: String, default: Boolean): Boolean {
        return prefs.getBoolean(name, default)
    }

    override fun putInt(name: String, value: Int) {
        prefs.edit().putInt(name, value).commit()
    }

    override fun getInt(name: String, default: Int): Int {
        return prefs.getInt(name, default)
    }

    override fun putString(name: String, value: String) {
        prefs.edit().putString(name, value).commit()
    }

    override fun getString(name: String, default: String): String {
        return prefs.getString(name, default)!!
    }
}

class FakeSharedPrefsRepo: SharedPrefsRepoIF {
    private val b = mutableMapOf<String, Boolean>()
    private val i = mutableMapOf<String, Int>()
    private val s = mutableMapOf<String, String>()

    override fun putBoolean(name: String, value: Boolean) {
        b[name] = value
    }

    override fun getBoolean(name: String, default: Boolean): Boolean {
        return b[name] ?: default
    }

    override fun putInt(name: String, value: Int) {
        i[name] = value
    }

    override fun getInt(name: String, default: Int): Int {
        return i[name] ?: default
    }

    override fun putString(name: String, value: String) {
        s[name] = value
    }

    override fun getString(name: String, default: String): String {
        return s[name] ?: default
    }
}