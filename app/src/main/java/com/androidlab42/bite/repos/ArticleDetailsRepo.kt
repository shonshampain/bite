package com.androidlab42.bite.repos

import com.androidlab42.bite.model.ApiManagerIF
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.model.nytimes.*
import com.androidlab42.bite.repos.FakeArticleDetailsRepoResponses.FAILURE
import com.androidlab42.bite.repos.FakeArticleDetailsRepoResponses.SUCCESS
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign

interface ArticleDetailsRepoIF {
    /**
     * Search the Article Details API for a given [url].
     * The [url] comes from an initial call to the Article List API.
     * Nothing is returned, as the call is reactive and will place results on the observable.
     */
    fun search(url: String)
    /**
     * The observable containing the search results.
     */
    val articleDetailsResponse: Observable<ArticleDetailsResult>
}

/**
 * [result] is a Boolean indicating whether the search was successful or not
 * in case [result] is false, [reason] can supply some error text
 * the actual results of the search are stored in [response]
 */
data class ArticleDetailsResult(
    val result: Boolean,
    val reason: String? = null,
    val response: ArticleDetailsResponse? = null
)

class ArticleDetailsRepo(private val apiManager: ApiManagerIF,
                         private val rxSchedulers: RxSchedulers
) : ArticleDetailsRepoIF {

    private val compositeDisposable = CompositeDisposable()

    private val _articleDetailsResponse: PublishRelay<ArticleDetailsResult> = PublishRelay.create()
    override val articleDetailsResponse: Observable<ArticleDetailsResult> = _articleDetailsResponse

    override fun search(url: String) {
        compositeDisposable += apiManager.nyTimesArticleSearchService.search("web_url:(\"$url\")")
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                _articleDetailsResponse.accept(
                    ArticleDetailsResult(
                        result = true,
                        response = it
                    )
                )
            }, {
                _articleDetailsResponse.accept(
                    ArticleDetailsResult(
                        result = false,
                        reason = it.message
                    )
                )
            })
    }
}

enum class FakeArticleDetailsRepoResponses { SUCCESS, FAILURE }

class FakeArticleDetailsRepo(private val response: FakeArticleDetailsRepoResponses = SUCCESS): ArticleDetailsRepoIF {

    private val _articleDetailsResponse: PublishRelay<ArticleDetailsResult> = PublishRelay.create()
    override val articleDetailsResponse: Observable<ArticleDetailsResult> = _articleDetailsResponse

    private lateinit var adr: ArticleDetailsResponse

    init {
        when(response) {
            SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "xlarge", url = "url")
                val headline1 = Headline(main = "headline")
                val doc1 = Doc("lead-paragraph", headline1, listOf(mm1, mm2,mm3), "abstract-1")
                val details = DetailsResponse(listOf(doc1))
                adr = ArticleDetailsResponse("status", "copyright", details)
            }
            else -> {}
        }
    }

    override fun search(url: String) {
        when(response) {
            SUCCESS -> {
                _articleDetailsResponse.accept(ArticleDetailsResult(result = true, response = adr))
            }
            FAILURE -> {
                _articleDetailsResponse.accept(ArticleDetailsResult(result = false, reason = "Something went wrong."))
            }
        }
    }
}