package com.androidlab42.bite.repos

import com.androidlab42.bite.model.ApiManagerIF
import com.androidlab42.bite.model.RxSchedulers
import com.androidlab42.bite.model.nytimes.Article
import com.androidlab42.bite.model.nytimes.Multimedia
import com.androidlab42.bite.model.nytimes.WireAPISearchResponse
import com.androidlab42.bite.repos.FakeArticleListRepoResponses.FAILURE
import com.androidlab42.bite.repos.FakeArticleListRepoResponses.SUCCESS
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber

interface ArticleListRepoIF {
    /**
     * Search the Article List API in a given [source] and [section].
     * Nothing is returned, as the call is reactive and will place results on the observable.
     */
    fun search(source: String, section: String)
    /**
     * The observable containing the search results.
     */
    val articleListResponse: Observable<ArticleListResult>
}

/**
 * [result] is a Boolean indicating whether the search was successful or not
 * in case [result] is false, [reason] can supply some error text
 * the actual results of the search are stored in [response]
 */
data class ArticleListResult(
    val result: Boolean,
    val reason: String? = null,
    val response: WireAPISearchResponse? = null
)

class ArticleListRepo(private val apiManager: ApiManagerIF,
                      private val rxSchedulers: RxSchedulers) : ArticleListRepoIF {

    private val compositeDisposable = CompositeDisposable()

    private val _articleListResponse: PublishRelay<ArticleListResult> = PublishRelay.create()
    override val articleListResponse: Observable<ArticleListResult> = _articleListResponse

    override fun search(source: String, section: String) {
        Timber.d("[search] repo: [$source] [$section]")
        compositeDisposable += apiManager.nyTimesWireAPIService.search(source, section)
            .observeOn(rxSchedulers.observe)
            .subscribeOn(rxSchedulers.subscribe)
            .subscribe({
                _articleListResponse.accept(
                    ArticleListResult(
                        result = true,
                        response = it
                    )
                )
            }, {
                _articleListResponse.accept(
                    ArticleListResult(
                        result = false,
                        reason = it.message
                    )
                )
            })
    }
}

enum class FakeArticleListRepoResponses { SUCCESS, FAILURE }

class FakeArticleListRepo(private val response: FakeArticleListRepoResponses = SUCCESS): ArticleListRepoIF {

    private val _articleListResponse: PublishRelay<ArticleListResult> = PublishRelay.create()
    override val articleListResponse: Observable<ArticleListResult> = _articleListResponse

    private lateinit var wasr: WireAPISearchResponse

    init {
        when(response) {
            SUCCESS -> {
                val mm1 = Multimedia(height = 1, width = 1, subtype = "subtype-1", url = "url-1")
                val mm2 = Multimedia(height = 2, width = 2, subtype = "subtype-2", url = "url-2")
                val mm3 = Multimedia(height = 3, width = 3, subtype = "subtype-3", url = "url-3")
                val article1 = Article("section-1", "subsection-1", "title-1", "abstract-1", "url-1", listOf(mm1, mm2,mm3))
                val article2 = Article("section-2", "subsection-2", "title-2", "abstract-2", "url-2", listOf(mm1, mm2,mm3))
                val article3 = Article("section-3", "subsection-3", "title-3", "abstract-3", "url-3", listOf(mm1, mm2,mm3))
                wasr = WireAPISearchResponse("status", "copyright", 500, listOf(article1, article2, article3))
            }
            else -> {}
        }
    }

    override fun search(source: String, section: String) {
        when(response) {
            SUCCESS -> {
                _articleListResponse.accept(ArticleListResult(result = true, response = wasr))
            }
            FAILURE -> {
                _articleListResponse.accept(ArticleListResult(result = false, reason = "Something went wrong."))
            }
        }
    }
}