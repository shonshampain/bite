Bite Interactive Code Challenge
===============================
By: Shon Shampain, shon.shampain@gmail.com

Requirements:

- Using any publicly available API of your choosing (examples include Yelp, Flickr, New York Times, etc.) build a simple, native Android app with at least two screens. One screen should show a list of items from that API while the second screen should show a detail view for those items. Use this as an opportunity to be creative and demonstrate your familiarity with the Android platform.

- Tests are required. They may take any form deemed appropriate for the app but should demonstrate a basic understanding of testing an Android app. Instrumented unit tests are not required.

- Avoid using 3rd party libraries. However, Retrofit, Dagger 2 - or your preferred dependency injector -, Glide, Google/ktx are allowed and encouraged. Essentially, if it's a de facto common platform library it's ok to use.

Implementation Notes:

- This application is a simple article reader tied into a couple of the NYTimes APIs.

- The min API is set at Android 5.0 Lollipop (API 21) which captures 89.3% of the users.
  For reference, see: https://developer.android.com/about/dashboards.
  
- There are many ways for view code to interact with XML, namely:
  findViewById(), ButterKnife and Kotlin Synthetics. 
  I feel like each of these methods is lacking in a certain regard, especially runtime cost and potential for error.
  The preferential method I use is via DataBinding.
  
- DataBinding is setup initially in the .xml file by using the <layout> tag,
  and then specifying any variables that will be used.
  It's worth noting that many useful things such as Context's can be passed into the VM
  from the .xml, which obviously solves a lot of issues.
  
- It is very important in my architectural philosophy to **not** use vanilla DataBinding,
  but rather to bind directly to the LiveData object !!
  
- I am not particularly advocating (or not) that my architectural view is "correct" in any sense of the word.
  What I **am** advocating, however, is that my architectural concepts are intentional,
  and well-thought out. To that end, I'm always striving to learn to do things better.
  As soon as I find a better way of doing things, I'll change.
  
- My concept of architecture begins with the concept of a "flow" which I define an an "idea"
  (for some definition of "idea") with encapsulated state. That is to say, the "flow" is an "idea"
  that handles, for all intents (pun intended) and purposes all things relevant
  to that idea; it is well-defined and it does not stray outside of its bounds. The entry point into
  a flow is an Activity, whose sole purpose is to setup the interaction between it and the xml and ViewModel, 
  handle things only the activity can (e.g. attaching an adapter to a RecyclerView)
  and handle Navigation. That's it; that is ALL the Activity lives for.
  
- The xml file links back to the ViewModel via a <layout> tag that binds <LiveData>.
  
- **NOTE** There was no good way to use a ViewState in this app b/c it is so simple.
           So, I have artificially used a ViewState in activity_reader.xml / ReaderViewModel.kt
           to demonstrate the concept.
             
- The ViewModels then are responsible for delegating all instances of business logic to a UseCase.

- A UseCase is a small, testable and encapsulated instance of business logic.

- The final pieces in the chain are repos which are responsible for holding data and
  communicating to off-devices resources.

- NB: LiveEvent is a class necessary to ensure that Navigation events (which should only happen once)
  do in fact only happen one time. This is just a necessity when using LiveData.
  
- All classes in the UseCase, Repo and Manager categories are supplied with a Fake.
  To understand the benefits of Faking vs. Mocking, please refer to:
  https://www.reddit.com/r/androiddev/comments/d05di8/do_you_use_the_impl_suffix_if_you_only_have_one/ez86bi3/
  
- A complete unit test suite for this app is provided.
  Right click on "test/java/com" and choose 'Run Tests in 'com''

- Unit testing is accomplished via Fakes at one level up.
  So ApiManager is not tested. (Because why test Retrofit? It's already well-tested.)
  The Repos are tested with the FakeApiManager.
  The UseCases are tested with the FakeRepos.
  The VMs are tested with the FakeUseCases.
  NB: By my convention, Fakes live in the same file in which they fake, to underscore the importance
      of keeping them up to date (as if failing tests were not enough). It's also a reminder to
      develop **with testing in mind** as opposed to something that occurs after the fact.
